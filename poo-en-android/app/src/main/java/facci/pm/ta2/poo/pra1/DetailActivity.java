package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6
        //////////////////
        //3.1
        //COMENTA QUE HACE

        String object_id =getIntent().getStringExtra("object_id");

        TextView description = (TextView) findViewById(R.id.descripcion);
        description.setMovementMethod(LinkMovementMethod.getInstance());

        // FIN - CODE6

        /////////////////
        // PREGUNTA 3.3

        DataQuery dataQuery = DataQuery.get("item");
        dataQuery.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {

                if (e == null){


                    /////////////////////
                    //Se buscan los TextView y la ImagenView a traves de su id.

                    TextView title = (TextView)findViewById(R.id.nombre);
                    TextView price = (TextView)findViewById(R.id.presio);
                    ImageView thumbnail = (ImageView)findViewById(R.id.thumbnail);
                    TextView description = (TextView)findViewById(R.id.descripcion);

                    /////////////////////
                   //Se coloca el string del title, description y el price.
                    // Tambien se coloca la imagen en el objeto thumbnail

                    title.setText((String) object.get("nombre"));
                    price.setText((String) object.get("presio")+"\u0024");
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));
                    description.setText((String) object.get("descripcion"));
                }else{
                   // GENERA ERROR
                }
            }
        });



        // FIN - CODE6

    }

}
